# NoNameBadge 2019 CTF

Collaborative NoNameBadge CTF Write-Up.

Suggest your write-ups as a merge request. Keep this filename convention: **challenge_dir/your_nickname_or_email.md**

[NoNameBadge Instruction page](https://www.nonamecon.org/badge)

[Official Leaderboard](https://spynet.techmaker.ua/scores)

# Contents

## Rooting

[Take a closer look at NoNameBadge Trailer](https://www.youtube.com/watch?v=chfoAWevHMs)

## Mr Bean Walker

 - [Anvol](Mr Bean Walker/anvol.md)

## BruteSearcher

 - [Anvol](BruteSearcher/anvol.md)

## NoNameCon SpyNet

 - [Anvol](NoNameCon SpyNet/anvol.md)

## Ployka PWNer

 - [Anvol](Ployka PWNer/anvol.md)

## Binary Hero

 - [Anvol](Binary Hero/anvol.md)

## Side Blennel

 - [Anvol](Side Blennel/anvol.md)